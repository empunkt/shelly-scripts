let CONNECTED_DEVICES_IDS = [0, 1, 2, 3];

let timer = undefined;

function main() {
  initConnectedDevices();

  Shelly.addEventHandler(eventHandler);
};

function initConnectedDevices() {
  for (let id in CONNECTED_DEVICES_IDS) {
    Shelly.call("Switch.SetConfig", {
      id: id,
      config: {
        in_mode: "detached",
        initial_state: "off"
      }
    });
  };
};

function eventHandler(event) {
  print(JSON.stringify(event));
  if (!canSwitch(event)) {
    print("Noting to do. Exiting.");

    return;
  };

  if (isInput(event)) {
    switchOffDelayed(event.id, 0);
  };
  // This doesn't make that much sense. Would be great to have kind of pre
  // switch hook that conditionally prevents the switch to be actually switched.
  //
  // else if (isSwitch(event)) {
  //   switchOffDelayed(event.id, 0.5);
  // };
};

function canSwitch(event) {
  let result = false;

  if (isConnectedDevice(event.id)) {
    print("Device #", JSON.stringify(event.id),
          " is not connected.");

    result = true;
  } else if (isToggleEvent(event)) {
    print("Event ", JSON.stringify(event), " is toggle event.");

    if (isInputOn(event.id)) {
      print("Input #", JSON.stringify(event.id), " is 'on'.");

      result = true;
    };
  };

  return result;
};

function isSwitch(event) {
  return (event.component.indexOf('switch') === 0);
};

function isInput(event) {
  return (event.component.indexOf('input') === 0);
};

function isConnectedDevice(deviceId) {
  for (let id in CONNECTED_DEVICES_IDS) {
    if (deviceId === id) {
      return true;
    };
  };

  return false;
};

function isToggleEvent(event) {
  return (event.info.event === "toggle");
};

function isInputOn(inputId) {
  print("Checking state of input #", JSON.stringify(inputId), ".");

  let isOn = undefined;

  let result = Shelly.getComponentStatus("input", inputId);

  print("State of input #", JSON.stringify(inputId), " is ",
        result.state ? "'on'" : "'off'", ".");

  return result.state;
};

function switchOffDelayed(switchId, delaySeconds) {
  print("Preparing switch #", JSON.stringify(switchId), " to turn off after ",
        JSON.stringify(delaySeconds), " seconds.");

  timer = Timer.set(delaySeconds * 1000, false, switchOff, switchId);
};

function switchOff(switchId) {
  print("Turning off switch #", JSON.stringify(switchId), ".");

  Shelly.call(
    "Switch.Set",
    {
      id: switchId,
      on: false
    },
    resetTimer
  );
};

function resetTimer() {
  print("Reset timer.")
  Timer.clear(timer);
  timer = undefined;
};

main();
